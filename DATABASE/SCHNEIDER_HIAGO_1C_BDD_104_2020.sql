-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 02, 2020 at 08:23 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `SCHNEIDER_HIAGO_1C_BD_104_2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `T_Adresse`
--

CREATE TABLE `T_Adresse` (
  `id_Adresse` int(11) NOT NULL,
  `Nom_Adresse` varchar(80) NOT NULL,
  `CodePostale` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Client`
--

CREATE TABLE `T_Client` (
  `id_Client` int(11) NOT NULL,
  `Nom_Client` varchar(50) NOT NULL,
  `Prenom_Client` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `T_Client`
--

INSERT INTO `T_Client` (`id_Client`, `Nom_Client`, `Prenom_Client`) VALUES
(1, 'Schneider', 'Hiago'),
(2, 'Roberto', 'Justos');

-- --------------------------------------------------------

--
-- Table structure for table `T_Client_Accepte_Mandat`
--

CREATE TABLE `T_Client_Accepte_Mandat` (
  `id_Client_Accepte_Mandat` int(11) NOT NULL,
  `FK_Client` int(11) NOT NULL,
  `FK_Mandat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Client_Avoir_Telephone`
--

CREATE TABLE `T_Client_Avoir_Telephone` (
  `id_Client_Avoir_Telephone` int(11) NOT NULL,
  `FK_Client` int(11) NOT NULL,
  `FK_Telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `T_Client_Avoir_Telephone`
--

INSERT INTO `T_Client_Avoir_Telephone` (`id_Client_Avoir_Telephone`, `FK_Client`, `FK_Telephone`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `T_Client_Habite_Adresse`
--

CREATE TABLE `T_Client_Habite_Adresse` (
  `id_Client_Habite_Adresse` int(11) NOT NULL,
  `FK_Client` int(11) NOT NULL,
  `FK_Adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Employe`
--

CREATE TABLE `T_Employe` (
  `id_Employe` int(11) NOT NULL,
  `Nom_Employe` varchar(50) NOT NULL,
  `Prenom_Employe` varchar(50) NOT NULL,
  `Poste_Employe` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Employe_Avoir_Telephone`
--

CREATE TABLE `T_Employe_Avoir_Telephone` (
  `id_Employe_Avoir_Telephone` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL,
  `FK_Telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Employe_Habite_Adresse`
--

CREATE TABLE `T_Employe_Habite_Adresse` (
  `id_Employe_Habite_Adresse` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL,
  `FK_Adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Mandat`
--

CREATE TABLE `T_Mandat` (
  `id_Mandat` int(11) NOT NULL,
  `Description_Mandat` varchar(500) NOT NULL,
  `EstimationTemps_Mandat` int(11) NOT NULL,
  `CoutEstime_Mandat` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Mandat_Collabore_Employe`
--

CREATE TABLE `T_Mandat_Collabore_Employe` (
  `id_Mandat_Collabore_Employe` int(11) NOT NULL,
  `FK_Mandat` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Mandat_ConduitPar_Employe`
--

CREATE TABLE `T_Mandat_ConduitPar_Employe` (
  `id_Mandat_ConduitPar_Employe` int(11) NOT NULL,
  `FK_Mandat` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Mandat_EtabliPar_Employe`
--

CREATE TABLE `T_Mandat_EtabliPar_Employe` (
  `id_Mandat_EtabliPar_Employe` int(11) NOT NULL,
  `FK_Mandat` int(11) NOT NULL,
  `FK_Employe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `T_Telephone`
--

CREATE TABLE `T_Telephone` (
  `id_Telephone` int(11) NOT NULL,
  `Numero_Telephone` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `T_Telephone`
--

INSERT INTO `T_Telephone` (`id_Telephone`, `Numero_Telephone`) VALUES
(1, 0),
(2, 78732465),
(4, 78732465),
(5, 77777777),
(795342915, 795342912),
(795342916, 786554935);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `T_Adresse`
--
ALTER TABLE `T_Adresse`
  ADD PRIMARY KEY (`id_Adresse`);

--
-- Indexes for table `T_Client`
--
ALTER TABLE `T_Client`
  ADD PRIMARY KEY (`id_Client`);

--
-- Indexes for table `T_Client_Accepte_Mandat`
--
ALTER TABLE `T_Client_Accepte_Mandat`
  ADD PRIMARY KEY (`id_Client_Accepte_Mandat`),
  ADD KEY `FK_Client` (`FK_Client`),
  ADD KEY `FK_Mandat` (`FK_Mandat`);

--
-- Indexes for table `T_Client_Avoir_Telephone`
--
ALTER TABLE `T_Client_Avoir_Telephone`
  ADD PRIMARY KEY (`id_Client_Avoir_Telephone`),
  ADD KEY `FK_Client` (`FK_Client`),
  ADD KEY `FK_Telephone` (`FK_Telephone`);

--
-- Indexes for table `T_Client_Habite_Adresse`
--
ALTER TABLE `T_Client_Habite_Adresse`
  ADD PRIMARY KEY (`id_Client_Habite_Adresse`),
  ADD KEY `FK_Client` (`FK_Client`),
  ADD KEY `FK_Adresse` (`FK_Adresse`);

--
-- Indexes for table `T_Employe`
--
ALTER TABLE `T_Employe`
  ADD PRIMARY KEY (`id_Employe`);

--
-- Indexes for table `T_Employe_Avoir_Telephone`
--
ALTER TABLE `T_Employe_Avoir_Telephone`
  ADD PRIMARY KEY (`id_Employe_Avoir_Telephone`),
  ADD KEY `FK_Employe` (`FK_Employe`),
  ADD KEY `FK_Telephone` (`FK_Telephone`);

--
-- Indexes for table `T_Employe_Habite_Adresse`
--
ALTER TABLE `T_Employe_Habite_Adresse`
  ADD PRIMARY KEY (`id_Employe_Habite_Adresse`),
  ADD KEY `FK_Employe` (`FK_Employe`),
  ADD KEY `FK_Adresse` (`FK_Adresse`);

--
-- Indexes for table `T_Mandat`
--
ALTER TABLE `T_Mandat`
  ADD PRIMARY KEY (`id_Mandat`);

--
-- Indexes for table `T_Mandat_Collabore_Employe`
--
ALTER TABLE `T_Mandat_Collabore_Employe`
  ADD PRIMARY KEY (`id_Mandat_Collabore_Employe`),
  ADD KEY `FK_Mandat` (`FK_Mandat`),
  ADD KEY `FK_Employe` (`FK_Employe`);

--
-- Indexes for table `T_Mandat_ConduitPar_Employe`
--
ALTER TABLE `T_Mandat_ConduitPar_Employe`
  ADD PRIMARY KEY (`id_Mandat_ConduitPar_Employe`),
  ADD KEY `FK_Mandat` (`FK_Mandat`),
  ADD KEY `FK_Employe` (`FK_Employe`);

--
-- Indexes for table `T_Mandat_EtabliPar_Employe`
--
ALTER TABLE `T_Mandat_EtabliPar_Employe`
  ADD PRIMARY KEY (`id_Mandat_EtabliPar_Employe`),
  ADD KEY `FK_Mandat` (`FK_Mandat`),
  ADD KEY `FK_Employe` (`FK_Employe`);

--
-- Indexes for table `T_Telephone`
--
ALTER TABLE `T_Telephone`
  ADD PRIMARY KEY (`id_Telephone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `T_Adresse`
--
ALTER TABLE `T_Adresse`
  MODIFY `id_Adresse` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Client`
--
ALTER TABLE `T_Client`
  MODIFY `id_Client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `T_Client_Accepte_Mandat`
--
ALTER TABLE `T_Client_Accepte_Mandat`
  MODIFY `id_Client_Accepte_Mandat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Client_Avoir_Telephone`
--
ALTER TABLE `T_Client_Avoir_Telephone`
  MODIFY `id_Client_Avoir_Telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `T_Client_Habite_Adresse`
--
ALTER TABLE `T_Client_Habite_Adresse`
  MODIFY `id_Client_Habite_Adresse` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Employe`
--
ALTER TABLE `T_Employe`
  MODIFY `id_Employe` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Employe_Avoir_Telephone`
--
ALTER TABLE `T_Employe_Avoir_Telephone`
  MODIFY `id_Employe_Avoir_Telephone` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Employe_Habite_Adresse`
--
ALTER TABLE `T_Employe_Habite_Adresse`
  MODIFY `id_Employe_Habite_Adresse` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Mandat`
--
ALTER TABLE `T_Mandat`
  MODIFY `id_Mandat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Mandat_Collabore_Employe`
--
ALTER TABLE `T_Mandat_Collabore_Employe`
  MODIFY `id_Mandat_Collabore_Employe` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Mandat_ConduitPar_Employe`
--
ALTER TABLE `T_Mandat_ConduitPar_Employe`
  MODIFY `id_Mandat_ConduitPar_Employe` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Mandat_EtabliPar_Employe`
--
ALTER TABLE `T_Mandat_EtabliPar_Employe`
  MODIFY `id_Mandat_EtabliPar_Employe` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `T_Telephone`
--
ALTER TABLE `T_Telephone`
  MODIFY `id_Telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=795342917;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `T_Client_Accepte_Mandat`
--
ALTER TABLE `T_Client_Accepte_Mandat`
  ADD CONSTRAINT `t_client_accepte_mandat_ibfk_1` FOREIGN KEY (`FK_Client`) REFERENCES `T_Client` (`id_Client`),
  ADD CONSTRAINT `t_client_accepte_mandat_ibfk_2` FOREIGN KEY (`FK_Mandat`) REFERENCES `T_Mandat` (`id_Mandat`);

--
-- Constraints for table `T_Client_Avoir_Telephone`
--
ALTER TABLE `T_Client_Avoir_Telephone`
  ADD CONSTRAINT `t_client_avoir_telephone_ibfk_1` FOREIGN KEY (`FK_Client`) REFERENCES `T_Client` (`id_Client`),
  ADD CONSTRAINT `t_client_avoir_telephone_ibfk_2` FOREIGN KEY (`FK_Telephone`) REFERENCES `T_Telephone` (`id_Telephone`);

--
-- Constraints for table `T_Client_Habite_Adresse`
--
ALTER TABLE `T_Client_Habite_Adresse`
  ADD CONSTRAINT `t_client_habite_adresse_ibfk_1` FOREIGN KEY (`FK_Client`) REFERENCES `T_Client` (`id_Client`),
  ADD CONSTRAINT `t_client_habite_adresse_ibfk_2` FOREIGN KEY (`FK_Adresse`) REFERENCES `T_Adresse` (`id_Adresse`);

--
-- Constraints for table `T_Employe_Avoir_Telephone`
--
ALTER TABLE `T_Employe_Avoir_Telephone`
  ADD CONSTRAINT `t_employe_avoir_telephone_ibfk_1` FOREIGN KEY (`FK_Employe`) REFERENCES `T_Employe` (`id_Employe`),
  ADD CONSTRAINT `t_employe_avoir_telephone_ibfk_2` FOREIGN KEY (`FK_Telephone`) REFERENCES `T_Telephone` (`id_Telephone`);

--
-- Constraints for table `T_Employe_Habite_Adresse`
--
ALTER TABLE `T_Employe_Habite_Adresse`
  ADD CONSTRAINT `t_employe_habite_adresse_ibfk_1` FOREIGN KEY (`FK_Adresse`) REFERENCES `T_Adresse` (`id_Adresse`),
  ADD CONSTRAINT `t_employe_habite_adresse_ibfk_2` FOREIGN KEY (`FK_Employe`) REFERENCES `T_Employe` (`id_Employe`);

--
-- Constraints for table `T_Mandat_ConduitPar_Employe`
--
ALTER TABLE `T_Mandat_ConduitPar_Employe`
  ADD CONSTRAINT `t_mandat_conduitpar_employe_ibfk_1` FOREIGN KEY (`FK_Mandat`) REFERENCES `T_Mandat` (`id_Mandat`),
  ADD CONSTRAINT `t_mandat_conduitpar_employe_ibfk_2` FOREIGN KEY (`FK_Employe`) REFERENCES `T_Employe` (`id_Employe`);

--
-- Constraints for table `T_Mandat_EtabliPar_Employe`
--
ALTER TABLE `T_Mandat_EtabliPar_Employe`
  ADD CONSTRAINT `t_mandat_etablipar_employe_ibfk_1` FOREIGN KEY (`FK_Mandat`) REFERENCES `T_Mandat` (`id_Mandat`),
  ADD CONSTRAINT `t_mandat_etablipar_employe_ibfk_2` FOREIGN KEY (`FK_Employe`) REFERENCES `T_Employe` (`id_Employe`);
